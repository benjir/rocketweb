<?php
/*
Template Name: App Configure JSON 
*/
  // echo "<pre>";
  // print_r($_POST);
if (isset($_POST['submit'])) {

  $data = array(
    'base_url'=>$_POST['base_url'],
    'navigation_bar'=>$_POST['navigation_bar'],
    'left_button_option'=>$_POST['left_button_option'],
    'right_button_option'=>$_POST['right_button_option'],
    'theme_color'=>$_POST['theme_color'],
    'loader_style'=>$_POST['loader_style'],
    'loader_int_time'=>(int)$_POST['loader_int_time'],
    'admob_id'=>$_POST['admob_id'],
    'key_ad_banner'=>$_POST['key_ad_banner'],
    'key_ad_interstitial'=>$_POST['key_ad_interstitial'],
    'key_ad_rewarded'=>$_POST['key_ad_rewarded'],
    'admob_delay'=>(int)$_POST['admob_delay'],
    'file_upload'=>(bool)$_POST['file_upload'],
    'photo_upload'=>(bool)$_POST['photo_upload'],
    'camera_photo_upload'=>(bool)$_POST['camera_photo_upload'],
    'multiple_file_upload'=>(bool)$_POST['multiple_file_upload'],
    'js_active'=>(bool)$_POST['js_active'],
    'downloads_webview'=>(bool)$_POST['downloads_webview'],
    'permission_dialog'=>(bool)$_POST['permission_dialog'],
    'splash_screen'=>(bool)$_POST['splash_screen']
  );
  // $data['configured_data'] = $given_data;
  // echo "<pre>";
  // print_r($data);
  // $json = json_encode($data);
}
?>

<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>RocketWeb || InfixSoft</title>

  <!-- Bootstrap core CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

</head>

<body>

  <!-- Page Content -->
  <div class="container mt-5">
    <div class="row">
      <div class="col-6">
        <div>
          <h3 class="mb-4">RocketWeb Configuaration Form</h3>
        </div>
        <form class="mt-2" action="<?php echo $_SERVER['PHP_SELF'];?>" method="POST">
          <div class="form-group">
            <label for="exampleFormControlInput1">01. Base URL (Ex: http://www.infixsoft.com)</label>
            <input type="url" pattern="https?://.+" name="base_url" class="form-control" id="exampleFormControlInput1" placeholder="http://www.infixsoft.com" required>
          </div>
          <div class="form-group">
            <label for="exampleFormControlSelect1">02. Select navigation bar style</label>
            <select class="form-control" name="navigation_bar" id="exampleFormControlSelect1">
              <option value="NAVIGATION_ORDINARY">NAVIGATION_ORDINARY</option>
              <option value="NAVIGATION_STANDER">NAVIGATION_STANDER</option>
              <option value="NAVIGATION_CLASSIC">NAVIGATION_CLASSIC</option>
              <option value="NAVIGATION_HIDE">NAVIGATION_HIDE</option>
            </select>
          </div>
          <div class="form-group">
            <label for="exampleFormControlSelect1">03. Select left button option</label>
            <select class="form-control" name="left_button_option" id="exampleFormControlSelect1">
              <option value="LEFT_MENU_HIDE">LEFT_MENU_HIDE</option>
              <option value="LEFT_MENU_SLIDER">LEFT_MENU_SLIDER</option>
              <option value="LEFT_MENU_RELOAD">LEFT_MENU_RELOAD</option>
              <option value="LEFT_MENU_SHARE">LEFT_MENU_SHARE</option>
              <option value="LEFT_MENU_HOME">LEFT_MENU_HOME</option>
            </select>
          </div>
          <div class="form-group">
            <label for="exampleFormControlSelect1">04. Select right button option</label>
            <select class="form-control" name="right_button_option" id="exampleFormControlSelect1">
              <option value="RIGHT_MENU_HIDE">RIGHT_MENU_HIDE</option>
              <option value="RIGHT_MENU_SLIDER">RIGHT_MENU_SLIDER</option>
              <option value="RIGHT_MENU_RELOAD">RIGHT_MENU_RELOAD</option>
              <option value="RIGHT_MENU_SHARE">RIGHT_MENU_SHARE</option>
              <option value="RIGHT_MENU_HOME">RIGHT_MENU_HOME</option>
            </select>
          </div>
          <div class="form-group">
            <label for="exampleFormControlSelect1">05. Select your theme and color</label>
            <select class="form-control" name="theme_color" id="exampleFormControlSelect1">
              <option value="AppThemePrimary">AppThemePrimary</option>
              <option value="AppThemeSecondary">AppThemeSecondary</option>
              <option value="AppThemeTurquoise">AppThemeTurquoise</option>
              <option value="AppThemeGreen">AppThemeGreen</option>
              <option value="AppThemeBlue">AppThemeBlue</option>
              <option value="AppThemeAmethyst">AppThemeAmethyst</option>
              <option value="AppThemeTasphalt">AppThemeTasphalt</option>
              <option value="AppThemeSunflower">AppThemeSunflower</option>
              <option value="AppThemeCarrot">AppThemeCarrot</option>
              <option value="AppThemeWhite">AppThemeWhite</option>
              <option value="AppThemeConcrete">AppThemeConcrete</option>
            </select>
          </div>
          <div class="form-group">
            <label for="exampleFormControlSelect1">06. Select loader style</label>
            <select class="form-control" name="loader_style" id="exampleFormControlSelect1">
              <option value="LOADER_ROTATING">LOADER_ROTATING</option>
              <option value="LOADER_DOUBLE_BOUNCE">LOADER_DOUBLE_BOUNCE</option>
              <option value="LOADER_WAVE">LOADER_WAVE</option>
              <option value="LOADER_WANDERING_CUBE">LOADER_WANDERING_CUBE</option>
              <option value="LOADER_PULSE">LOADER_PULSE</option>
              <option value="LOADER_CHASING_DOTS">LOADER_CHASING_DOTS</option>
              <option value="LOADER_THREE_BOUNCE">LOADER_THREE_BOUNCE</option>
              <option value="LOADER_CIRCLE">LOADER_CIRCLE</option>
              <option value="LOADER_CUBE_GRID">LOADER_CUBE_GRID</option>
              <option value="LOADER_FADING_CIRCLE">LOADER_FADING_CIRCLE</option>
              <option value="LOADER_FOLDING_CUBE">LOADER_FOLDING_CUBE</option>
              <option value="LOADER_ROTATING_CIRCLE">LOADER_ROTATING_CIRCLE</option>
              <option value="LOADER_HIDE">LOADER_HIDE</option>
            </select>
          </div>
          <div class="form-group">
            <label for="exampleFormControlSelect1">07. Set your loader interval time in milliseconds <br> (1000 milliseconds = 1 second)</label>
              <input type="number" min="1000" name="loader_int_time" class="form-control" placeholder="1000">
          </div>
          <div class="form-group">
            <label for="exampleFormControlSelect1">08. Set your adMob ID <br>(To hide adMob in the app set KEY_AD_MOB value as "" (empty)  )</label>
            <input type="text" name="admob_id" class="form-control" placeholder="adMob ID">
          </div>
          <div class="form-group">
            <label for="exampleFormControlSelect1">09. Set adMob key ad banner</label>
            <input type="text" name="key_ad_banner" class="form-control" placeholder="adMob key ad banner">
          </div>
          <div class="form-group">
            <label for="exampleFormControlSelect1">10. Set adMob key ad interstitial</label>
            <input type="text" name="key_ad_interstitial" class="form-control" placeholder="adMob key ad interstitial">
          </div>
          <div class="form-group">
            <label for="exampleFormControlSelect1">11. Set adMob key ad rewarded</label>
            <input type="text" name="key_ad_rewarded" class="form-control" placeholder="adMob key ad rewarded">
          </div>
          <div class="form-group">
            <label for="exampleFormControlSelect1">12. Set adMob delay in milliseconds <br> (1000 milliseconds = 1 second)</label>
              <input type="number" min="1000" name="admob_delay" class="form-control" placeholder="1000">
          </div>
          <div class="form-group">
            <legend class="col-form-label">13. Set file upload enable</legend>
            <div class="form-check form-check-inline">
              <input class="form-check-input" type="radio" name="file_upload" id="exampleRadios1" value="1" checked>
              <label class="form-check-label " for="exampleRadios1">True</label>
            </div>
            <div class="form-check form-check-inline">
              <input class="form-check-input" type="radio" name="file_upload" id="exampleRadios1" value="0" >
              <label class="form-check-label " for="exampleRadios1">False</label>
            </div>
          </div>
          <div class="form-group">
            <legend class="col-form-label">14. Set photo upload enable</legend>
            <div class="form-check form-check-inline">
              <input class="form-check-input" type="radio" name="photo_upload" id="exampleRadios1" value="1" checked>
              <label class="form-check-label " for="exampleRadios1">True</label>
            </div>
            <div class="form-check form-check-inline">
              <input class="form-check-input" type="radio" name="photo_upload" id="exampleRadios1" value="0" >
              <label class="form-check-label " for="exampleRadios1">False</label>
            </div>
          </div>
          <div class="form-group">
            <legend class="col-form-label">15. Set camera photo upload enable</legend>
            <div class="form-check form-check-inline">
              <input class="form-check-input" type="radio" name="camera_photo_upload" id="exampleRadios1" value="1" checked>
              <label class="form-check-label " for="exampleRadios1">True</label>
            </div>
            <div class="form-check form-check-inline">
              <input class="form-check-input" type="radio" name="camera_photo_upload" id="exampleRadios1" value="0" >
              <label class="form-check-label " for="exampleRadios1">False</label>
            </div>
          </div>
          <div class="form-group">
            <legend class="col-form-label">16. Set multiple file upload enable</legend>
            <div class="form-check form-check-inline">
              <input class="form-check-input" type="radio" name="multiple_file_upload" id="exampleRadios1" value="1" checked>
              <label class="form-check-label " for="exampleRadios1">True</label>
            </div>
            <div class="form-check form-check-inline">
              <input class="form-check-input" type="radio" name="multiple_file_upload" id="exampleRadios1" value="0" >
              <label class="form-check-label " for="exampleRadios1">
              False
              </label>
            </div>
          </div>
          <div class="form-group">
            <legend class="col-form-label">17. Set javascript active</legend>
            <div class="form-check form-check-inline">
              <input class="form-check-input" type="radio" name="js_active" id="exampleRadios1" value="1" checked>
              <label class="form-check-label " for="exampleRadios1">True</label>
            </div>
            <div class="form-check form-check-inline">
              <input class="form-check-input" type="radio" name="js_active" id="exampleRadios1" value="0" >
              <label class="form-check-label " for="exampleRadios1">False</label>
            </div>
          </div>
          <div class="form-group">
            <legend class="col-form-label">18. Set downloads in webview active</legend>
            <div class="form-check form-check-inline">
              <input class="form-check-input" type="radio" name="downloads_webview" id="exampleRadios1" value="1" checked>
              <label class="form-check-label " for="exampleRadios1">True</label>
            </div>
            <div class="form-check form-check-inline">
              <input class="form-check-input" type="radio" name="downloads_webview" id="exampleRadios1" value="0" >
              <label class="form-check-label " for="exampleRadios1">
              False
              </label>
            </div>
          </div>
          <div class="form-group">
            <legend class="col-form-label">19. Set permssion dialog active</legend>
            <div class="form-check form-check-inline">
              <input class="form-check-input" type="radio" name="permission_dialog" id="exampleRadios1" value="1" checked>
              <label class="form-check-label " for="exampleRadios1">True</label>
            </div>
            <div class="form-check form-check-inline">
              <input class="form-check-input" type="radio" name="permission_dialog" id="exampleRadios1" value="0" >
              <label class="form-check-label " for="exampleRadios1">False</label>
            </div>
          </div>
          <div class="form-group">
            <legend class="col-form-label">20. Set splash screen active</legend>
            <div class="form-check form-check-inline">
              <input class="form-check-input" type="radio" name="splash_screen" id="exampleRadios1" value="1" checked>
              <label class="form-check-label " for="exampleRadios1">True</label>
            </div>
            <div class="form-check form-check-inline">
              <input class="form-check-input" type="radio" name="splash_screen" id="exampleRadios1" value="0" >
              <label class="form-check-label " for="exampleRadios1">
              False
              </label>
            </div>
          </div>
          <div class="form-group mt-3">
             <input name="submit" class="btn btn-primary" type="submit" value="Get JSON">
          </div>
        </form>
      </div>
      <div class="col-6">
        <div class="form-group mt-5">
          <label for="exampleFormControlSelect1">( Your configured JSON output )</label>
          <textarea class="form-control" id="exampleFormControlTextarea1" rows="15">
            <?php
            if (!empty($data)) {
             $json=json_encode($data,JSON_UNESCAPED_SLASHES);
              echo $json;
            }
            ?>
          </textarea>
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript -->
  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

</body>

</html>

