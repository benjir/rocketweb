<!DOCTYPE html>
<html>
<head>
	<title>Sample Envato Purchase Code Verifier</title>
	<!-- <link rel="stylesheet" type="text/css" href="style.css"> -->
</head>
<style>
a,abbr,acronym,address,applet,article,aside,audio,b,big,blockquote,body,canvas,caption,center,cite,code,dd,del,details,dfn,div,dl,dt,em,embed,fieldset,figcaption,figure,footer,form,h1,h2,h3,h4,h5,h6,header,hgroup,html,i,iframe,img,ins,kbd,label,legend,li,mark,menu,nav,object,ol,output,p,pre,q,ruby,s,samp,section,small,span,strike,strong,sub,summary,sup,table,tbody,td,tfoot,th,thead,time,tr,tt,u,ul,var,video{margin:0;padding:0;border:0;font-size:100%;font:inherit;vertical-align:baseline}article,aside,details,figcaption,figure,footer,header,hgroup,menu,nav,section{display:block}body{line-height:1}ol,ul{list-style:none}blockquote,q{quotes:none}blockquote:after,blockquote:before,q:after,q:before{content:'';content:none}table{border-collapse:collapse;border-spacing:0}body{font-family:Arial;font-size:16px}.wrap{background:#eee;max-width:370px;margin:10% auto 0 auto;padding:5em 5em 5em 2.5em;box-shadow:0 3px 10px 0 #efefef;border-radius:5px;border:1px solid #ddd}table{border:1px solid #222}table td{border:1px solid #333}input[type=text]{padding:20px;display:block;width:100%;font-size:1em}input[type=submit]{padding:15px 20px;background:#81b441;display:inline-block;border:0 none;color:#fff;font-size:1em;border-radius:2px}label{margin-bottom:10px;display:block;color:#777}
</style>
<body>
	<div class="wrap">
		<!-- // Include the EnvatoPurchaseCodeVerifier.php file that holds the verifier class.-->
		<?php require_once 'EnvatoPurchaseCodeVerifier.php'; ?>
		<!-- // Create your own access token at (https://build.envato.com/create-token)-->
		<?php $access_token = 'eAgPE7gtBInttKUdYtVdbQjUiWCXayeq'; // Change 'your_token' value with your own token ?>
		<!-- // Create new instance of EnvatoPurchaseCodeVerifier. -->
		<!-- // Pass your own access token -->
		<?php $purchase = new EnvatoPurchaseCodeVerifier($access_token); ?>

		<?php $buyer_purchase_code = filter_input(INPUT_POST, 'purchase_code', FILTER_DEFAULT); ?>

		<!-- check if user submits the form -->
		<?php if ( ! empty( $buyer_purchase_code ) ) { ?>

			<?php $verified = $purchase->verified($buyer_purchase_code); ?>
			<?php echo '<pre>';print_r($verified); die();?>
			<!-- User purchase code is good!-->
			<?php if ( $verified ) { ?>
				<?php
					 $item_id = $verified['item']['id'];
					 $item_name = $verified['item']['name'];
					 $buyer = $verified['buyer'];
					 $license = $verified['license'];
					 $amount = $verified['amount'];
					 $sold_at = $verified['sold_at'];
					 // $supported_until = $verified['supported_until'];
				 ?>
				 <h1>Valid Purchase Code</h1>
				 <table>
					<tr><td>Item ID:</td><td><?php echo $item_id; ?></td></tr>
					<tr><td>Item Name:</td><td><?php echo $item_name; ?></td></tr>
					<tr><td>Buyer:</td><td><?php echo $buyer; ?></td></tr>
					<tr><td>License:</td><td><?php echo $license; ?></td></tr>
					<tr><td>Amount:</td><td><?php echo $amount; ?></td></tr>
					<tr><td>Sold At:</td><td><?php echo $sold_at; ?></td></tr>
				</table>
			<?php } else { ?>
				<!-- Invalid purchase code -->
				<h1>Invalid Purchase Code</h1>
			<?php } ?>

		<?php } else  { ?>
			<form action="<?php echo $_SERVER['PHP_SELF'];?>" method="post">
				<label>Purchase Code: </label>
				<input type="text" name="purchase_code" placeholder="Type or paste the buyer's purchase code here"><br>
				<input type="submit">
			</form>
		<?php } ?>


	</div>
</body>
</html>
