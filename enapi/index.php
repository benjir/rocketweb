<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Page Title</title>
</head>
<body>
<?php

    session_start();
    if (isset($_GET['code']) && !empty($_GET['code'])) {
        $url = 'https://api.envato.com/token';
        $_SESSION['theUriRequestSentFrom'] = $_GET['state'];
        $fields = array(
    	    'grant_type' => 'authorization_code',
    	    'code' => $_GET['code'],
    	    'client_id' => 'enapi-yhe8r6mh',
    	    'client_secret' => '8w16ianRaH2MO6vq9xqD5iIYe8UZREce'
        );
    
        //url-ify the data for the POST
        foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
        rtrim($fields_string, '&');
    
        //open connection
        $ch = curl_init();
    
        //set the url, number of POST vars, POST data
        curl_setopt($ch,CURLOPT_URL, $url);
        curl_setopt($ch,CURLOPT_POST, count($fields));
        curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
    
        //execute post
        $result = curl_exec($ch);
        //close connection
        curl_close($ch);
        $data = json_decode($result, true);
        $_SESSION['access_token'] = $data['access_token'];
    }

    if (isset($_SESSION['access_token']) && !empty($_SESSION['access_token'])) {
        // Get cURL resource
        $curl = curl_init();
        // Set some options - we are passing in a useragent too here
        curl_setopt_array($curl, [
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => 'https://api.envato.com/v1/market/private/user/username.json',
            CURLOPT_USERAGENT => 'Codular Sample cURL Request',
            CURLOPT_HTTPHEADER => array(
                'Authorization: Bearer '.$_SESSION['access_token']
            )
        ]);
        // Send the request & save response to $resp
        $resp = curl_exec($curl);
        // Close request to clear up some resources
        curl_close($curl);
        $user = json_decode($resp, true);
        $_SESSION['logge_in_username'] = $user['username'];
    }
    
    if (isset($_SESSION['logge_in_username']) && !empty($_SESSION['logge_in_username'])) {
        // Get cURL resource
        $curl = curl_init();
        // Set some options - we are passing in a useragent too here
        curl_setopt_array($curl, [
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => 'https://api.envato.com/v1/market/user:'.$_SESSION['logge_in_username'].'.json',
            CURLOPT_USERAGENT => 'Codular Sample cURL Request',
            CURLOPT_HTTPHEADER => array(
                'Authorization: Bearer '.$_SESSION['access_token']
            )
        ]);
        // Send the request & save response to $resp
        $resp = curl_exec($curl);
        // Close request to clear up some resources
        curl_close($curl);
        $user_details = json_decode($resp, true);
        echo '<pre>';
        print_r($user_details);
    }
        if (isset($_SESSION['logge_in_username']) && !empty($_SESSION['logge_in_username'])) {
        // Get cURL resource
        $curl = curl_init();
        // Set some options - we are passing in a useragent too here
        curl_setopt_array($curl, [
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => 'https://api.envato.com/v1/market/private/user/email.json',
            CURLOPT_USERAGENT => 'Codular Sample cURL Request',
            CURLOPT_HTTPHEADER => array(
                'Authorization: Bearer '.$_SESSION['access_token']
            )
        ]);
        // Send the request & save response to $resp
        $resp = curl_exec($curl);
        // Close request to clear up some resources
        curl_close($curl);
        $user_details = json_decode($resp, true);
        echo '<pre>';
        print_r($user_details);
    }
    if (isset($_SESSION['logge_in_username']) && !empty($_SESSION['logge_in_username'])) {
        // Get cURL resource
        $curl = curl_init();
        // Set some options - we are passing in a useragent too here
        curl_setopt_array($curl, [
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => 'https://api.envato.com/v3/market/buyer/list-purchases?include_all_item_details=false',
            CURLOPT_USERAGENT => 'Codular Sample cURL Request',
            CURLOPT_HTTPHEADER => array(
                'Authorization: Bearer '.$_SESSION['access_token']
            )
        ]);
        // Send the request & save response to $resp
        $resp = curl_exec($curl);
        // Close request to clear up some resources
        curl_close($curl);
        $user_details = json_decode($resp, true);
        $itemIdArray = array();
        $item_results= $user_details['results'];
        foreach ($item_results as $item_result) {
            $itemIdArray[] = $item_result['item']['id'];
        }
        echo '<pre>';
        print_r($itemIdArray);
        echo '<br>';
        $product_item_id = 5086341;
        if(in_array($product_item_id,$itemIdArray)){
           header("Location: ".$_SESSION['theUriRequestSentFrom']);
        } else {
           echo "<h3>Product id  not Found</h3>";
        }
    }
?>



</body>
</html>